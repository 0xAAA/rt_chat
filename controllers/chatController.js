'use strict';
var config = require('../config/config.js');
var mongo = require( '../config/database.js' );

function chatController()
{
    //Constructor

	//Private
	var that = this;

	// Client connections
	var cli = [];
	
	//Nickname for each client
	var nickname = []; 

	//Priviledge
	// Get all connected clients
	this.getClients = function ()
	{
		return cli;
	};

	// Store a new client
	this.addClient = function (conn)
	{
		cli[conn.id] = conn;
	};

	// Delete a client that is no longer connected
	this.deleteClient = function (conn)
	{
		delete cli[conn.id];
		delete nickname[conn.id];
	};

	// Set a nickname for a client
	this.setNickname = function (conn, msg)
	{
		if(msg.nickname !== undefined) {
			
			var find = false;

			// Check if nickname already exist
			for (var key in nickname) {
				if (nickname[key] == msg.nickname) {
					find = true;
					return;
				}
			}

			// If nickname does not exit, create it
			if (!find) {
				nickname[conn.id] = msg.nickname;
			}
		}
	};

	this.getNickname = function (id)
	{
		return nickname[id];
	};
}

//Public
chatController.prototype.broadcast = function (conn, msg)
{
	var cli = this.getClients();

	// Set a nickname if required
	this.setNickname(conn, msg);
	
	// Get nickname
	var user = (this.getNickname(conn.id) === undefined)? conn.id  : this.getNickname(conn.id);

	var res = {nickname: user, txt:msg.txt};

	var db = mongo.getDb();
	
	//We should create de collection as capped
	//db.createCollection( "messages",{capped:true, size:100000, max:200} )
	//insert record
	db.collection('messages').insertOne(res, function(err, records) {
		if (err) throw err;
		console.log("Record added as "+records);
	});
	
	for (var key in cli)
	{
		cli[key].write(JSON.stringify(res));
	}

};



module.exports = chatController;
