'use strict';

var express = require('express');
var app = express();
var helper = require('./helper.js');
var config = require('./config/config.js');
var mongo = require( './config/database.js' );

/*
app.use(function (request, response, next) 
{
    //middleware
});
*/

app.use(express.static('client'));


app.get('/v0/messages', function (request, response)
{
    var db = mongo.getDb();

    db.collection('messages').find().toArray(function (err, collection)
    {
        if (err) throw err;
        response.send(collection);
    });
});

app.get('/v0/messages/:count', function (request, response)
{
    var db = mongo.getDb();

    var numOfMsgs = parseInt(request.params.count);

    db.collection('messages').find().limit(numOfMsgs).toArray(function (err, collection)
    {
        if (err) throw err;
        response.send(collection);
    });
});

app.use(function(req, res, next) {
    res.status(404).send('Uoops! Page not found!');
});

module.exports = app;

