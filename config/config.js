var config =
{
    db: {
        name: 'chat',
        address: 'localhost',
        port: '27017',
        user: '',
        pwd: ''
    },
    http:{
        port: 8080,
    },
    websocket:{
        port: 8081,
    }
};

module.exports = config;
