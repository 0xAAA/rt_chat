var config = require('./config.js');
var MongoClient = require( 'mongodb' ).MongoClient;

var _db;

module.exports = {

    conexionString: function () {
        return 'mongodb://'+config.db.address+':'+config.db.port+'/'+config.db.name;
    },
    connectToServer: function( callback ) 
    {
        MongoClient.connect(this.conexionString(), function( err, db ) {
            _db = db;
            return callback( err );
        } );
    },

    getDb: function() {
        return _db;
    }
};