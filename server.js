'use strict';
var app = require('./routes.js');
var mongo = require( './config/database.js' );
var config = require('./config/config.js');
var sockjs = require('sockjs');
var chatController = require('./controllers/chatController.js');

// SockJS server
var sockjs_opts = {sockjs_url: "http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js"};

var socket = sockjs.createServer(sockjs_opts);
var chat = new chatController();
var name = 0;

socket.on('connection', function(conn)
{
   ++name;
   chat.addClient(conn);
   
   conn.on('data', function(msg)
   {
      msg = JSON.parse(msg);

      var that = this;
      
      chat.broadcast(that, msg);
   });

   conn.on('close', function()
   {
      var that = this;

      chat.deleteClient(that);
   });
});

// Mongo Server
mongo.connectToServer( function( err ) 
{
   console.log('connecting Mongo DB ...');
   if (err) throw err;
   console.log('connected to Mongo DB');
});

// HTTP Server
var server = app.listen(config.http.port, function ()
{
   var host = server.address().address;
   var port = server.address().port;
   
   console.log("Server listening at http://%s:%s", host, port);
});


socket.installHandlers(server, {prefix:'/chat'});